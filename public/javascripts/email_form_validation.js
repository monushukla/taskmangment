$.validator.addMethod("noSpace", function(value, element) { 
    return value == '' || value.trim().length != 0;  
  }, "No space please and don't leave it empty");

$("#myform").validate({
    ignore: [],
              debug: false,
    rules: {
        name:{
            required:true,
            noSpace:true,
            minlength:2,
            maxlength:20
        },
        subject: {
            required: true,
            noSpace:true,
            minlength:2,
            maxlength:30
        },
        content:{
            required: function() 
           {
            CKEDITOR.instances.content.updateElement();
           },
           noSpace:true,
            minlength:5,
            maxlength:300
       },
       role:{
           required:true,
           noSpace:true,
           minlength:2,
           maxlength:20
       },
       policy:{
           required:true,
           noSpace:true,
           minlength:2,
           maxlength:30
       }
         },
    messages: {
        name:{
            required:'please enter the name',
            minlength:'please not enter minimum two words',
            maxlength:'please not enter more then 20 words'
        } ,
        subject:{
            required:'please enter the subject',
            minlength:'please not enter minimum two words',
            maxlength:'please not enter more then 30 words'
        } ,
        content:{
            required:'please enter the content',
            minlength:'please enter maximum 5 words',
            maxlength:'please not enter more then 300 words'
        } ,
        role:{
            required:'please enter the role',
            minlength:'please enter maximum 2 words',
            maxlength:'please not enter more then 20 words'

        },
        policy:{
            required:'please enter the policy',
            minlength:'please enter maximum 2 words',
            maxlength:'please not enter more then 20 words'

        }, 
    }
});
$(document).on("click", ".button-ho", function () {		//INSERT AND DELETE THE INPUT FIELD FOR HOBBIES
    if ($(this).text() == "+") {
        $(".addrow").append("<div><input type='text' name='policy' class='form-control textfield' placeholder='enter you policy '><button type='button' class='btn btn-danger button-ho'>-</button></div>");
    }
    else {
        $(this).parent().remove();
    }
});