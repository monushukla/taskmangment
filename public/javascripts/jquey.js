$(document).ready(() => {
    $('.view').on('click', function () {

        let link = $(this).attr('link');
        console.log(link)
        $.ajax({
            method: "GET",
            url: link,
            success: function (result) {
                $("#name").text(result.user_name);
                $("#email").text(result.email);
                $("#number").text(result.phone_no);
                $("#role").text(result.role);
                console.log(result)
            }
        })
        $('#myModal').modal('show');
    });
    $(document).on('click', '.del', function () {
        console.log(this)
        let link = $(this).attr('link');
        let pagevalue = $(this).attr('page');
        console.log(pagevalue)
        console.log(link)
        bootbox.confirm({
            message: "do you want to delete",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    $.ajax({
                        method: "PUT",
                        url: link,
                        data: { pageNo: pagevalue },
                        dataType: 'JSON',
                        success: function (result) {
                            console.log("result");
                            window.location.href = '/admin/user_role?pageNo=' + pagevalue;
                        }
                    })
                }
            }
        });

    });
    $(document).on('click', '.delete', function () {
        console.log(this)
        let link = $(this).attr('link');
        let pagevalue = $(this).attr('page');
        var whichtr = $(this).closest("tr");
        console.log(whichtr)
        console.log(pagevalue)
        console.log(link)
        bootbox.confirm({
            message: "do you want to delete",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    $.ajax({
                        method: "DELETE",
                        url: link,
                        success: function (result) {
                            if(result.status==200){
                                console.log("result");
                                whichtr.remove();
                                window.location.reload();
                               // window.location.href = '/admin/show_user?pageNo='+pagevalue;
                            }
                          
                        }
                    });
                }
            }
        });
       
        
    });
    $(document).on('click', '.task_del', function () {
        console.log(this)
        let link = $(this).attr('link');
        let pagevalue = $(this).attr('page');
        console.log(pagevalue)
        console.log(link)
        bootbox.confirm({
            message: "do you want to delete",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    $.ajax({
                        method: "DELETE",
                        url: link,
                        success: function (result) {
                           window.location.href = '/user?pageNo='+pagevalue;
                        }
                    })
                }
            }
        });
       
    });
 

})

