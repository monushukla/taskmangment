const express = require('express');
const router = express.Router();
const emailcontroller=require('./controller')
const email=new emailcontroller();
const mongoose = require('mongoose');
router.get('/',  async(req, res) => {
    let data = req.query;
    template=await email.template(data)
      res.render('email/email_table',{active: "email_template", data: template,pic:req.user});
  });
  router.get('/add_template',(req,res)=>{
    res.render('email/email_template',{ active: "email_template",message: req.flash(),pic:req.user});
  })
  router.post('/', (req, res, next) => {
      email.addtemplate(req.body).then(result=>{
          res.redirect('/email_template')
      }).catch(err => {
        req.flash("error", 'template already exists')
        res.redirect('/email_template/add_template')
      });    
  });
  router.get('/email_edit/:id', (req, res) => {
    let user_id = mongoose.Types.ObjectId(req.params.id)
    email.get_data(user_id).then(result => {
    res.render('email/email_template_edit', {active: "email_template", data: result,pic:req.user});
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
  });
  router.post('/email_edit/:id', (req, res) => {
    email.update_data(req.params.id, req.body).then(result => {
     res.redirect('/email_template');
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
  });
  router.put('/email_del/:id', (req, res) => {
    email.remove_data(req.params.id).then(result => {
      req.flash('success', 'template deleted sucessfully');
     res.redirect('/email_template')
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
  });

module.exports = router;