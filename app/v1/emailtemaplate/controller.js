var mongoose = require('mongoose');
const emial_modal = require('./email_modal');
class email {
   async addtemplate(data){
       try{
        let find_user = await emial_modal.findOne({ name: data.name });
        if (!find_user) {
        let data1 = await emial_modal.create(data);
        return Promise.resolve(data1)
        }
        else{
            return Promise.reject()
        }
       }catch(err){
           console.log(err);
       }
    }
    async template(data) {
        try {
            let pagenumber=[];
            let pageNo = 1, size = 5;
            if (data.pageNo) {
                pageNo = parseInt(data.pageNo)
                console.log("pageNo")
                console.log(pageNo)
            }
            let skip = (pageNo - 1) * size;
             let list = await emial_modal.find().skip(skip).limit(size);

            let countUsers = await emial_modal.find().count();
            let allPages = parseInt(countUsers / size);
            if (countUsers % size != 0) {
                allPages++;
            }
            for(let i=1;i<=allPages;i++){
                pagenumber.push(i)
            }
            list.current = pageNo
            if (pageNo == 1) {
                list.prev = 1
            }
            else {
            list.prev = pageNo - 1
            }
            list.pageNo=pagenumber;
            list.next = pageNo + 1
            list.last = allPages
            list.count = countUsers
            list.sr = (pageNo - 1) * size + 1
            
            return Promise.resolve(list);
        } catch (err) {
            console.log(err);
        }
    }
    async get_data(id) {
        try {
            let data = await emial_modal.findOne({ _id:id });
            return Promise.resolve(data);
        } catch (err) {
            console.log(err);
        }
    }
    async update_data(id, user_data) {
        try {

            let data = await emial_modal.updateOne({ _id:id }, user_data);
            return Promise.resolve(data);
        } catch (err) {
            console.log(err);
        }

    }
    async remove_data(id) {
        try {
            let data = await emial_modal.deleteOne({ _id: id });
            return Promise.resolve(data);
        } catch (err) {
            console.log(err);
        }

    }
}
module.exports = email;