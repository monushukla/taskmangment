'use strict';
const user_role = require('./modal.js');
const user_detailes = require('./user_modal')
const email_template = require('../emailtemaplate/email_modal')
const passwordHash = require('password-hash');
const nodemailer = require("nodemailer");
const csv = require('fast-csv');
const fs = require("fs");

class User1 {
    async list(data) {
        try {
            let pagenumber = [];
            let pageNo = 1, size = 5;
            if (data.pageNo) {
                pageNo = parseInt(data.pageNo)
                console.log("pageNo")
                console.log(pageNo)
            }
            let skip = (pageNo - 1) * size;
            let query = { 'role': { $ne: 'Admin' }, isdeleted: 0 }

            let list = await user_role.find(query).skip(skip).limit(size).sort({ 'role': 1 });

            let countUsers = await user_role.find(query).count();
            let allPages = parseInt(countUsers / size);
            if (countUsers % size != 0) {
                allPages++;
            }
            for (let i = 1; i <= allPages; i++) {
                pagenumber.push(i)
            }
            list.current = pageNo
            if (pageNo == 1) {
                list.prev = 1
            }
            else {
                list.prev = pageNo - 1
            }
            list.pageNo = pagenumber;
            list.next = pageNo + 1
            list.last = allPages
            list.count = countUsers
            list.sr = (pageNo - 1) * size + 1

            return Promise.resolve(list);
        } catch (err) {
            console.log(err);
        }
    }

    async add_user_role(data) {
        try {
            let find = await user_role.findOne({ role: data.role })
            console.log("find");
            console.log(find);
            if (!find) {
                let data1 = await user_role.create(data);
                return Promise.resolve(data1)
            }
            else {
                return Promise.reject()
            }


        } catch (err) {
            console.log(err);
        }
    }
    async edit_role(id) {
        try {
            let data = await user_role.findOne({ _id: id }); //await user_role.findById(id);
            if (data) {
                return Promise.resolve(data);
            }
            else {
                return Promise.reject()
            }


        } catch (err) {
            console.log(err);
        }

    };
    async update_role(id, user_data) {
        try {
            let data = await user_role.updateOne({ _id: id }, user_data);
            return Promise.resolve(data);
        } catch (err) {
            console.log(err);
        }
    }

    async user_role_deleted(id) {
        try {

            let data = await user_role.updateOne({ _id: id }, { $set: { isdeleted: 1 } });

            return Promise.resolve(data);
        } catch (err) {
            console.log(err);
        }

    }
    async list_role() {
        try {
            let query = { 'role': { $ne: 'Admin' }, isdeleted: 0 }
            let data = await user_role.find(query)

            return Promise.resolve(data)
        } catch (err) {
            console.log(err);
        }
    }
    async add_user_detail(data) {
        try {
            let find_user = await user_detailes.findOne({ email: data.email });
            if (!find_user) {
                let random_number = Math.random().toString(36).substring(7);
                let password_hash = passwordHash.generate(random_number);
                data.password = password_hash;
                let data1 = await user_detailes.create(data);
                if (data1) {
                    let emailvalue = await email_template.findOne({ "name": "Useradd" });
                    let content = emailvalue.content;
                    // let res = content.replace(/@email|@password/gi, data.email,random_number);
                    let mapObj = {
                        verfiy_email: data.email,
                        verfiy_password: random_number,
                        varify_link: "http://localhost:3000/"
                    };
                    let res = content.replace(/verfiy_email|verfiy_password|varify_link/gi, function (matched) {
                        return mapObj[matched];
                    });
                    console.log(res);
                    let transporter = nodemailer.createTransport({
                        host: "smtp.gmail.com",
                        port: 587,
                        auth: {
                            user: 'smonu5444@gmail.com', // generated ethereal user
                            pass: '97949758' // generated ethereal password
                        }
                    });
                    // send mail with defined transport object
                    let info = transporter.sendMail({
                        to: "smonu5444@gmail.com", // list of receivers
                        subject: emailvalue.subject, // Subject line
                        html: res//'<html><body><p><h4>yourEmail:-' + data.email + '</h4><br><h4>yourpassword:-' + random_number + '<h4></p></body></html>'
                    });
                    console.log("Message sent: %s", info.messageId);
                    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
                    return Promise.resolve(data1);
                }
            }
            else {
                return Promise.reject();
            }
        } catch (err) {
            console.log(err);
        }
    }
    async userlist(pagedata) {
        try {
            let pageNo = 1, size = 5, pagenumber = [];
            if (pagedata.pageNo) {
                pageNo = parseInt(pagedata.pageNo)
                console.log("pageNo")
                console.log(pageNo)
            }
            let skip = (pageNo - 1) * size;
            let query = [
                { $match: { 'role': { $ne: "Admin" } } },
                {
                    $lookup:
                    {
                        from: 'user_roles',
                        let: { local: '$role' },
                        pipeline: [{ $match: { $expr: { $eq: ['$role', '$$local'] } } }],
                        as: 'myCustomResut'
                    }
                },

                {
                    $project:
                        { _id: 1, role: 1, email: 1, created_at: 1, user_name: 1, status: 1, 'myCustomResut.policy': 1 }
                },
                { $sort: { "user_name": 1 } }, { $skip: skip }, { $limit: size }
            ];
            let data = await user_detailes.aggregate(query);
            let countUsers = await user_detailes.find().count();
            let allPages = parseInt(countUsers / size);
            if (countUsers % size != 0) {
                allPages++;
            }

            for (let i = 1; i <= allPages; i++) {
                pagenumber.push(i)
            }
            data.current = pageNo
            if (pageNo == 1) {
                data.prev = 1
            }
            else {
                data.prev = pageNo - 1
            }
            data.pageNo = pagenumber;
            data.next = pageNo + 1
            data.last = allPages
            data.count = countUsers
            data.sr = (pageNo - 1) * size + 1


            // let query = [
            //     { $lookup: { from: 'user_roles', localField: ObjectId('role'), foreignField: '_id', as: 'myCustomResut' } },
            //     {$project:{_id:1,role:1,email:1,created_at:1,user_name:1,"myCustomResut.police":1}}
            //   ]

            //   const util = require('util')
            //   console.log(util.inspect(query, false, null, true /* enable colors */))

            //   let data = await user_detailes.aggregate(query);
            //    console.log(util.inspect(data, false, null, true /* enable colors */))
            console.log(data)
            return Promise.resolve(data)
        } catch (err) {
            console.log(err);
        }

    }
    async edit_detailes(id) {
        try {
            let match = await user_detailes.findOne({ _id: id });
            if(match){ 
            let data = {}
            data.result = await user_detailes.findOne({ _id: id }); //await user_role.findById(id);
            let query = { 'role': { $ne: 'Admin' }, isdeleted: 0 }
            let result = await user_role.find(query)
            console.log("result");
            console.log(result);
            data.edit = result;
            data.pass = 10;
            console.log("data")
            console.log(data);
            return Promise.resolve(data);
            } else{
                return Promise.reject();
            }

        } catch (err) {
            console.log(err);
        }

    };
    async update_detelies(id, user_data) {
        console.log("id")
        console.log(id)
        try {
            let data = await user_detailes.updateOne({ _id: id }, user_data);
            return Promise.resolve(data);
        } catch (err) {
            console.log(err);
        }
    }
    async user_detelies_deleted(id) {
        console.log("id")
        console.log(id)
        try {
            let data = await user_detailes.deleteOne({ _id: id });
            return Promise.resolve(data);
        } catch (err) {
            console.log(err);
        }
    }
    async status(id, data) {
        try {
            console.log(id, data)
            let res = await user_detailes.updateOne({ _id: id }, data);
            console.log(res)
            return Promise.resolve();
        } catch (err) {
            console.log(err);
        }

    }
    async view_user_data(id) {
        try {
            let data = await user_detailes.findOne({ _id: id });

            return Promise.resolve(data);
        } catch (error) {
            console.log(error)
        }
    }
    async forgate_password_sendlink(data) {
        try {


            let forgate = await user_detailes.findOne({ email: data.email });
            let emailvalue = await email_template.findOne({ "name": "forgetpassword" });
            console.log("email_template")
            console.log(emailvalue)
            let content = emailvalue.content;
            // var res = content.replace(/@email|@password/gi, data.email,random_number);
            let mapObj = {
                verifypassword: "http://localhost:3000/password/" + forgate._id,
                varify_link: "http://localhost:3000/"
            };
            let res = content.replace(/verifypassword|varify_link/gi, function (matched) {
                return mapObj[matched];
            });
            console.log(res)
            let transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 587,
                auth: {
                    user: 'smonu5444@gmail.com', // generated ethereal user
                    pass: '97949758' // generated ethereal password
                }
            });
            // send mail with defined transport object
            let info = transporter.sendMail({
                to: "smonu5444@gmail.com", // list of receivers
                subject: emailvalue.subject, // Subject line
                html: '<html><body><p><a href="http://localhost:3000/password/' + forgate._id + '/">reset your password</a></p></body></html>'
            });
            console.log("Message sent: %s", info.messageId);
            console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

            return Promise.resolve(forgate);
        } catch (err) {
            console.log(err);

        }

    }
    async set_password(id, data) {
        try {
            let find_user = await user_detailes.findOne({ _id: id });
            console.log(find_user);
            if (find_user) {
                let password_hash = passwordHash.generate(data.reset_pasword);
                console.log(password_hash);
                let set_pass = await user_detailes.updateOne({ _id: id }, { $set: { password: password_hash } });
                return Promise.resolve(set_pass);
            }

        } catch (err) {
            console.log(err);
        }

    }
    async reset_password( data) {
        try {
            let find_user = await user_detailes.findOne({ email: data.email });
            console.log(find_user);
            if (find_user) {
                let password_hash = passwordHash.generate(data.reset_pasword);
                console.log(password_hash);
                let set_pass = await user_detailes.updateOne({ email: data.email }, { $set: { password: password_hash } });
                return Promise.resolve(set_pass);
            }

        } catch (err) {
            console.log(err);
        }

    }
    async export_csv() {
        try {
            let csv_data = await user_detailes.find({ is_deleted: 0 }, { user_name: 1, role: 1, email: 1, address: 1, phone_no: 1 });
            return new Promise((resolve, reject) => {
                const csvStream = csv.format({ headers: true });

                csv_data = JSON.parse(JSON.stringify(csv_data));
                const ws = fs.createWriteStream('/var/www/html/myapp2/app/v1/dashboard/out123' + Date.now() + '.csv');

                csvStream.pipe(ws)
                    .on('finish', function (data) {
                        console.log("writing finish")
                        resolve(ws.path);
                    });
                csv_data.forEach(obj => {
                    csvStream.write(obj)
                });
                csvStream.end()
            }
            )
        } catch (err) {
            console.log(err);
        }
    };

    async upload_csv(file) {
        try {
           
          new Promise((resolve, reject) => {
              
            let result, type, array = [];
                fs.createReadStream(file.destination + '/' + file.filename)
                    .pipe(csv.parse({ headers: true }))
                    .on('data', async (data) => {
                        array.push(data);
                              

                    }).on('end', async () => {
                        console.log('CSV file successfully processed');
                        let result = await user_detailes.find({});
                        let query = { 'role': { $ne: 'Admin' }, isdeleted: 0 }
                        let type = await user_role.find(query)
                        let roletype = false;
                        let matches1 = false;
                        let matches = [];
                       
                        for (let i = 0; i < array.length; i++) {
                            matches1 = false;
                            roletype = false;
                            for (let e = 0; e < result.length; e++) {
                                if (array[i].email === result[e].email)
                                    matches1 = true;
                                //matches.push( array[i] );
                                // console.log("matches");
                                // console.log(matches);
                            }
                            if (!matches1) {
                                for (let k = 0; k < type.length; k++) {
                                    if (array[i].role === type[k].role)
                                        roletype = true;


                                }
                            }
                            if (roletype) {
                                matches.push(array[i]);

                                
                            }
                        }
                        let insertuser =await user_detailes.create(matches)
                        console.log("insertuser");
                        console.log(insertuser);
                        for (let m = 0; m < insertuser.length; m++) 
                        {
                        let transporter = nodemailer.createTransport({
                            host: "smtp.gmail.com",
                            port: 587,
                            auth: {
                                user: 'smonu5444@gmail.com', // generated ethereal user
                                pass: '97949758' // generated ethereal password
                            }
                        });
                        // send mail with defined transport object
                        let info = await transporter.sendMail({
                            to: matches[m].email, // list of receivers
                            subject: "reset password", // Subject line
                            // text: data1.email + '\n' + random_number + "\n" + 'http://localhost:3000/email/.id',
                          html: '<html><body><p><a href="http://localhost:3000/password/' +insertuser[m]._id + '/">reset your password</a></p></body></html>'

                        });
                        console.log("Message sent: %s", info.messageId);
                        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
                   }
                        console.log("matches");
                        console.log(email);
                        resolve(insertuser)
                    });
                 // return Promise. resolve(result)

            });
        } catch (err) {
            console.log(err);
        }
    }

    async verfiy_email(data) {
        try {
            let verfiy = await user_detailes.find({ email: data });
            console.log(verfiy.length);
            if (verfiy.length === 0) {
                return Promise.reject()
            }
            else {
                return Promise.resolve(verfiy)
            }
        } catch (err) {
            console.log(err);
        }
    }




}

module.exports = User1;