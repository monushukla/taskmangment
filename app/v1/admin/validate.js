const { check, validationResult } = require('express-validator');

class uservalidate {
    validateHandler(req, res, next) {
        try{
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            next();
        } else {
            console.log(errors);
           // res.status(400).send({ message: "error in field", errors: errors });
        }
    } catch(err){
        console.log(err);
    }
    }
    get loginValidate() {
        try {
        return [
            check('name').not().isEmpty().withMessage('please enter your email'),
            check('password').not().isEmpty().withMessage('please enter your password')
        ]
    } catch(err){
        console.log(err);
    }
    }
    get user_role() {
        try {
        return [
            check('role').not().isEmpty().withMessage('please enter the  user role'),
            check('policy').not().isEmpty().withMessage('please enter the role policy')
        ]
    } catch(err){
        console.log(err);
    }
    }
    get user_information(){
        try{
        return [
            check('user_name').not().isEmpty().withMessage('please enter the user name'),
            check('email').not().isEmpty().withMessage('please enter your email'),
            check('phone_no').not().isEmpty().withMessage('please enter your phonenumber'),
            check('role').not().isEmpty().withMessage('please select the user role'),
            check('address').not().isEmpty().withMessage('please enter the user address'),
            check('salary').not().isEmpty().withMessage('please enterthe user salary'),
            ]
        } catch(err){
            console.log(err)
        }
    }
}
  

module.exports = uservalidate;