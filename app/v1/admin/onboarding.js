var express = require('express');
var router = express.Router();
const passport = require('passport')
var passwordHash = require('password-hash');
const LocalStrategy = require('passport-local').Strategy
const user = require('./user_modal')
var login = require('./controller')
var objUser = new login()
const validate = require('./validate');
const validation = new validate();
var pass_id;



// /* show login page on / router */
router.get(['/', '/login'], (req, res, next) => {
   res.render('login_email.html', { message: req.flash() });
});
router.post('/login', (req, res, next) => {
   req.body = req.body.name.toLowerCase();
   console.log(req.body);
   objUser.verfiy_email(req.body).then(result => {
      res.render('login_email_pass.html', { data: result[0] });
   }).catch(err => {
      req.flash("error", 'user not exists')
      res.redirect('/')
   });
})
router.get('/password', (req, res) => {
   res.render('login_email_pass')
})


// //login user account
router.post('/authenticate', validation.loginValidate, validation.validateHandler,
   passport.authenticate('local', {
      failureRedirect: '/login',
   }), (req, res, user_data) => {
      try {
         if (!user_data) { //if user not found (incorrect usernmae or password)
            req.flash('error', 'Invalid credentials');
            res.redirect('/');
         }
         //check for users
         if (user_data.is_deleted == 1) {
            req.flash('error', 'your account deleted by admin');
            res.redirect('/')
         }
         req.flash('success', 'welcome to the dashboard');
         res.redirect('/dashboard');

      } catch (err) {
         res.status(400).send({ message: err.message, status: 0 });
      }
      console.log(req.user)
   });


router.get(['/forgetpassword'], (req, res, next) => {
   res.render('forgetpassword.html', { message: req.flash() });
});
// //forgate pasword
router.post('/forget', (req, res, next) => {
   objUser.forgate_password_sendlink(req.body).then(result => {
      res.redirect('/');
   }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
});
router.get('/reset_pass', (req, res) => {
   res.render('reset_password', { message: req.flash() });

});
// //verify reset password
router.get('/password/:id', (req, res, next) => {
   pass_id = req.params.id;
   res.redirect('/reset_pass');
});
// //change password
router.post("/reset_pasword", (req, res, next) => {
   console.log("pass_id,req.body");
   console.log(pass_id, req.body);
   objUser.set_password(pass_id, req.body).then(result => {
      req.flash('success', 'your password change successfully');
      res.redirect('/');
   }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
})
router.get('/csv_reset_password',(req,res)=>{
   res.render('csv_password.html');
})
router.post("/csv_reset_pasword", (req, res, next) => {
   objUser.reset_password(req.body).then(result => {
      req.flash('success', 'your password change successfully');
      res.redirect('/');
   }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
})

passport.use(new LocalStrategy({ usernameField: 'name', passwordField: 'password', passReqToCallback: true },
   async (req, name, password, done) => {
      try {
         let data = await user.findOne({ email: name, status: 1 });


         if (!data) {
            return done(null, false);
         }
         if (!passwordHash.verify(password, data.password)) {
            return done(null, false);
         }

         return done(null, data);
      } catch (err) {
         console.log("error in passport");
         console.log(err);
      }


   }));

passport.serializeUser((user, done) => {
   done(null, user._id);
});

passport.deserializeUser(async (id, done) => {
   let user_data = await user.findOne({ _id: id });
   user_data = JSON.parse(JSON.stringify(user_data));
   done(null, user_data);
});


module.exports = router;
