var mongoose = require('mongoose');


var schema=mongoose.Schema;
var userdetails = new schema({
    user_name: { type: String },
    role: { type: String,ref:'user_roles'},
    email:{type:String},
    phone_no:{type:Number},
    address:{type:String},
    salary:{type:Number,default:2000},
    password:{type:String},
    is_deleted:{type:String,default:'0'},
    profile_pic:{type:String},
    status:{type:Number,default:1},
     },{timestamps:{createdAt:'created_at',updatedAt:'upadte_at'}});
       module.exports= mongoose.model('user_details', userdetails);
       
