const express = require('express');
const router = express.Router();
const user1 = require('./controller')
const mongoose = require('mongoose');
const multer = require('multer');
const authvalidation=require('../middleware/auth');
const auth=new authvalidation();
const path = require('path');
const validate = require('./validate')
const validation = new validate();
const objUser = new user1();
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname, '/../../../public/upload'));
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + file.originalname)
  }

})
const upload = multer({ storage: storage })

router.get('/user_role', async (req, res) => {
  let data = req.query
  let list_data = await objUser.list(data)
  res.render('admin/user_role', { active: "user_role", message: req.flash(), data: list_data, pic: req.user });
})
router.get('/add_user_role',auth.admin, (req, res) => {
  res.render('admin/add_user_role', { active: "user_role", pic: req.user, message: req.flash() });
})
router.post('/user_role_data', validation.user_role, validation.validateHandler, (req, res) => {
  objUser.add_user_role(req.body).then(result => {
    req.flash('success', 'user role added succefully');
    res.redirect('/admin/user_role')
  }).catch(err => {
    req.flash('error', 'user role allready exists')
    res.redirect('/admin/add_user_role')
  });
});
router.get('/edit_user_role_data/:id', (req, res) => {
  let user_id = mongoose.Types.ObjectId(req.params.id)
  objUser.edit_role(user_id).then(result => {
    res.render('admin/edit_user_role', { active: "user_role", data: result, pic: req.user, message: req.flash() })
  }).catch(err => {
    res.redirect('/admin/user_role')
    //res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});
router.post('/edit_user_role_data/:id', validation.user_role, validation.validateHandler, (req, res) => {
  objUser.update_role(req.params.id, req.body).then(result => {
    req.flash('success', 'data has been changed')
    res.redirect('/admin/user_role')
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});
router.put('/user_role_del/:id',auth.admin, (req, res) => {
  objUser.user_role_deleted(req.params.id).then(result => {
    req.flash('success', 'role deleted sucessfully');
    res.send();
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});
router.get('/show_user', async (req, res) => {

  try {
    let data = req.query
    let user_list = await objUser.userlist(data);
    res.render("admin/user_detailes", { active: "employee", data: user_list, pic: req.user, message: req.flash() })
  } catch (err) {
    console.log(err)
  }
});
router.get('/add_user',auth.admin, (req, res) => {

  objUser.list_role().then(result => {
    res.render("admin/add_user", { active: "employee", data: result, pic: req.user, message: req.flash() })
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});
router.post('/user_role_details', validation.user_information, validation.validateHandler, (req, res) => {
  objUser.add_user_detail(req.body).then(result => {
    req.flash('success', 'user added succefully');
    res.redirect('/admin/show_user')
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
})
router.get('/edit_user_detailes/:id', (req, res) => {
  let user_id = mongoose.Types.ObjectId(req.params.id);
  objUser.edit_detailes(user_id).then(result => {
    res.render('admin/edit_user', { active: "employee", data: result, pic: req.user, message: req.flash() })
  }).catch(err => {
    res.redirect('/admin/show_user')
   // res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
})
router.post('/edit_user_detailes/:id', validation.user_information, validation.validateHandler, (req, res) => {
  objUser.update_detelies(req.params.id, req.body).then(result => {
    req.flash('success', 'user data has been changed');
    res.redirect('/admin/show_user')
  }).catch(err => {
   res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});
router.get('/view/:id', function (req, res, next) {
  objUser.view_user_data(req.params.id).then(result => {
    res.send(result);
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});
router.delete('/del_user_detailes/:id',auth.admin, (req, res) => {
  objUser.user_detelies_deleted(req.params.id).then(result => {
    req.flash('success', 'user deleted sucessfully');
    res.send({ status: 200 });
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});
router.put('/status/:id', (req, res) => {
  objUser.status(req.params.id, req.body).then(() => {
    res.send({ status: 200 });
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});
router.get('/export', async (req, res) => {
  objUser.export_csv().then(result => {
    res.download(result);
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
})
router.get('/import',auth.admin, (req, res) => {
  res.render('admin/import_csv', { active: "employee", pic: req.user });
});
router.post('/upload_csv', upload.single('file'), (req, res, ) => {
  objUser.upload_csv(req.file).then(result => {
    res.redirect('/admin/show_user')
  }).catch(err => {
    req.flash('error', '')
  });
})
router.get('/socket', (req, res) => {
  res.render('chat.html', { active: "socket_chat", pic: req.user })
})


module.exports = router;
