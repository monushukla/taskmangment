class Auth{
    admin(req,res,next){

        if(req.user.role === 'Admin'){
            next();
        }
        else{
            res.redirect('/dashboard')
        }
    }
    hr(req,res,next){
        if(req.user.role==="Admin"|| req.user.role==='hr'){
            next();
        }
        else{
            res.redirect('/dashboard')
        }
    }
    user(req,res,next){
        if(req.user.role==="hr"){
            res.redirect('/dashboard')
        }
        else{
            next();
        }
    }
    login(req,res,next){
        if(req.user){
            res.redirect('/dashboard')
        }
        else{
            res.redirect('/')
        }
    }
}
module.exports=Auth;