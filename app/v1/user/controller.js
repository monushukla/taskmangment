const task = require('./user_modal')
const mongoose = require('mongoose');


class addtask {
    async task_list(data) {
        try {
            let pagenumber = [];
            let pageNo = 1, size = 5;
            if (data.pageNo) {
                pageNo = parseInt(data.pageNo)
                console.log("pageNo")
                console.log(pageNo)
            }
            let skip = (pageNo - 1) * size;
           

            let list = await task.find({}).skip(skip).limit(size);

            let countUsers = await task.find({}).count();
            let allPages = parseInt(countUsers / size);
            if (countUsers % size != 0) {
                allPages++;
            }
            for (let i = 1; i <= allPages; i++) {
                pagenumber.push(i)
            }
            list.current = pageNo
            if (pageNo == 1) {
                list.prev = 1
            }
            else {
                list.prev = pageNo - 1
            }
            list.pageNo = pagenumber;
            list.next = pageNo + 1
            list.last = allPages
            list.count = countUsers
            list.sr = (pageNo - 1) * size + 1
            return Promise.resolve(list)
        } catch (err) {
            console.log(err)
        }
    }
    async addtask_data(data) {
        try {
            let find_user = await task.findOne({ project_name: data.project_name });
            if (find_user == null) {
                let project = await task.create(data)
                return Promise.resolve(project)
            }
            else {
                return Promise.reject()
            }

        } catch (err) {
            Console.log(err);
        }
    }
    async edit_detailes(id) {
        try {
            let data = await task.findOne({ _id: id }); //await user_role.findById(id);
            console.log(data)
            return Promise.resolve(data);
        } catch (err) {
            console.log(err);
        }

    };
    async update_detelies(id, user_data) {
        console.log("id")
        console.log(id)
        try {
            let data = await task.updateOne({ _id: id }, user_data);
            return Promise.resolve(data);
        } catch (err) {
            console.log(err);
        }
    }
    async user_detelies_deleted(id) {
        console.log("id")
        console.log(id)
        try {
            let data = await task.deleteOne({ _id: id });
            return Promise.resolve(data);
        } catch (err) {
            console.log(err);
        }
    }
    async status(id, data) {
        try {
            console.log(id, data)
            let res = await task.updateOne({ _id: mongoose.Types.ObjectId(id) }, data);
            console.log(res)
            return Promise.resolve();
        } catch (err) {
            console.log(err);
        }

    }
}
module.exports = addtask;