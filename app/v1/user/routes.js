const express = require('express');
const router = express.Router();
let mongoose = require('mongoose')
const addtask = require('./controller');
const task = new addtask();



router.get('/', async (req, res) => {
    let data=req.query;
    let list = await task.task_list(data)
    res.render('user/user_task', { data: list, pic: req.user, active: 'add_task', message: req.flash() })
})
router.get('/addtask', (req, res) => {
    res.render('user/addtask', { active: 'add_task', pic: req.user })
});
router.post('/user_task', (req, res) => {
    task.addtask_data(req.body).then(result => {
        res.redirect('/user')
    }).catch(err => {
        req.flash('error', 'project allready exists')
        res.redirect('/user/addtask')
        console.log(errr)
    })
})
router.get('/task/:id', (req, res) => {
    let user_id = mongoose.Types.ObjectId(req.params.id);
    task.edit_detailes(user_id).then(result => {
        res.render('user/edit_task', { active: 'add_task', data: result, pic: req.user, message: req.flash() })
    }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
})
router.post('/task_edit/:id', (req, res) => {
    task.update_detelies(req.params.id, req.body).then(result => {
        req.flash('success', 'project data has been changed');
        res.redirect('/user')
    }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
});
router.delete('/user_del/:id', (req, res) => {
    task.user_detelies_deleted(req.params.id).then(result => {
        req.flash('success', 'project deleted sucessfully');
        res.send();
    }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
});
router.put('/status/:id', (req, res) => {
    task.status(req.params.id, req.body).then(() => {
      res.send({status:200});
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 1 });
    });
  });

module.exports = router;