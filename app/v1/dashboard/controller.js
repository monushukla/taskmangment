
const user = require('../admin/user_modal');
const passwordHash = require('password-hash');

class dashboard_user {
    async countRecord() {
        let query={'role':{$ne:'Admin'}}
        let data = await user.find(query).count();
        return Promise.resolve(data);
    }
    async change_password(user_data, data) {
        try {
            let result = passwordHash.verify(data.old_pasword, user_data.password)
            if (result) {
                let password_hash = passwordHash.generate(data.new_pasword);
                console.log(user_data._id);
                let set_pass = await user.updateOne({ _id: user_data._id }, { $set: { password: password_hash } });
                console.log("set_pass")
                console.log(set_pass)
                return Promise.resolve(set_pass);
            }
        } catch (err) {
            console.log(err);
        }
    }
    async update_data(user_data, data) {
        try {
            console.log(data);
            let user_upadte = await user.updateMany({ _id: user_data._id }, data);
            console.log("hello3")
            return Promise.resolve(user_upadte);
        } catch (err) {
            console.log(err);
        }

    }
    async profile_pic(user_data, data) {
        try {
            let update_pic = await user.updateMany({ _id: user_data._id }, { $set: { profile_pic: data.url } });
                return Promise.resolve(update_pic)
         
        } catch (err) {
            console.log(err);
        }
    }
   


}
module.exports = dashboard_user;
