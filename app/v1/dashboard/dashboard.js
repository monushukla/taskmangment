const express = require('express');
const router = express.Router();
const login = require('./controller')
const dashboard_class = new login();

router.post('/profile',  (req, res, next) => {
   dashboard_class.profile_pic(req.user, req.body).then(result => {
    res.redirect("/dashboard/chnage_profile")
  })

})

router.get('/', (req, res) => {
  try {
    dashboard_class.countRecord().then(result => {
      res.render('count_user', { active: "dashboard", count: result, pic: req.user, message: req.flash() })
    })

  } catch (err) {
    console.log("err");
    console.log(err);
  }
});
router.get('/chnage_profile', (req, res) => {
  res.render('changprofile', {active: "dashboard", pic: req.user });
});
router.post('/update_profile', function (req, res, next) {
  dashboard_class.update_data(req.user, req.body).then(result => {
    res.redirect('/dashboard');
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});
router.get('/chanage_password', (req, res) => {
  res.render('change_password',{active: "dashboard",});
});


router.post('/password', (req, res) => {
  dashboard_class.change_password(req.user, req.body).then(result => {
    req.flash('success', 'change password successfully');
    res.redirect('/dashboard');
  })
})
router.get('/logout',  (req, res, next) =>{
  if (req.session) {
    // delete session object
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});



 module.exports = router;
