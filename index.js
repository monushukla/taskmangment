const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const swig = require('swig');
const db = require('./connection.js');
const passport = require('passport')
const flash = require('connect-flash');
const session = require('express-session')
const staticify = require('staticify')(path.join(__dirname, 'public'));
const acl=require('./app/v1/middleware/auth');
const auth= new acl();
const app = express();

/**
 * Module dependencies.
 */
const port_number = require('./config');
const debug = require('debug')('myapp2:server');
const http = require('http');


/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || port_number.port);
app.set('port', port);

/**
 * Create HTTP server.
 */

const server = http.createServer(app);
const io = require('socket.io')(server)


/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}


db.connect();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('trust proxy', 1);

app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());

app.use(session({ name: 'demo', resave: false, secret: '123' }));
app.use(passport.initialize());
app.use(passport.session());
app.use(session({ cookie: { maxAge: 10000 } }));
app.use(flash());
app.use(staticify.middleware);
app.locals = {
  getVersionedPath: staticify.getVersionedPath
};
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app')));
// app.use((req, res, next) => {
//   if (req.user) {
//     res.redirect('/user')
//   }
//   else {
//     next();
//   }
// })
app.use('/',require('./app/v1/admin/onboarding'));
app.use((req, res, next) => {
  if (req.user) {
    next();
  }
  else {
    res.redirect('/')
  }
})
app.use('/admin', require('./app/v1/admin/routes'));
app.use('/email_template',auth.admin, require('./app/v1/emailtemaplate/emailroutes'));
app.use('/dashboard', require('./app/v1/dashboard/dashboard'));
app.use('/user',auth.user, require('./app/v1/user/routes'));
let users = {}
io.on('connection', socket => {
  socket.on('new_user', (data, cb) => {
 
    if (data in users) {
      cb(false);
    }
    else {
      cb(true);
      socket.nickname = data;
      users[socket.nickname] = socket;
      updateNickenames();
    }
  })
  function updateNickenames() {
    io.sockets.emit('user_name', Object.keys(users))
  }
  socket.on('send_message', (data) => {
    const msg = data.trim();
    if (msg.substr(0, 3) === '/w ') {
      msg = msg.substr(3);
      const ind = msg.indexOf(' ');
      if (ind !== -1) {
        const name = msg.substring(0, ind);
        msg = msg.substring(ind + 1);
        if (name in users) {
          users[name].emit('whisper', { msg: msg, nick: socket.nickname });
          users[socket.nickname].emit('whisper', { msg: msg, nick: socket.nickname });
        } else {
          cb('Error! enter a valid user')
        }
      } else {
        cb('Error! enter a valid message')
      }
    } else {
      io.sockets.emit('new_message', { msg: msg, nick: socket.nickname })
    }
  });
  socket.on('disconnect', (data) => {
    if (!socket.nickname) return;
    delete users[socket.nickname];
    updateNickenames();
  });
});



// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  //console.log(err);
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
